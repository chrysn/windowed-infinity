# windowed-infinity ![License: MIT OR Apache-2.0](https://img.shields.io/badge/license-MIT%20OR%20Apache--2.0-blue) [![windowed-infinity on crates.io](https://img.shields.io/crates/v/windowed-infinity)](https://crates.io/crates/windowed-infinity) [![windowed-infinity on docs.rs](https://docs.rs/windowed-infinity/badge.svg)](https://docs.rs/windowed-infinity) [![Source Code Repository](https://img.shields.io/badge/Code-On%20Codeberg-blue?logo=Codeberg)](https://codeberg.org/chrysn/windowed-infinity) ![Rust Version: 1.75.0](https://img.shields.io/badge/rustc-1.75.0-orange.svg)

This crate provides the [WindowedInfinity][__link0] struct and implementations of various traits to
write to it.

Its primary purpose is to wrap a small buffer such that writes to it advance a cursor over a larger
imaginary buffer, only persisting writes to the small buffer. After the buffer has been
processed, a new WindowedInfinity can be set up and the writing process repeated. This is
wasteful when the writes are computationally expensive, but convenient when operations only
rarely exceed the buffer.

A typical practical example of a WindowedInfinity application is the implementation of CoAP
block-wise transfer according to [RFC7959][__link1]; a simpler
example is available in the `demo.rs` example.

### Features

The set of traits implemented by [WindowedInfinity][__link2] depends on the configured cargo features:

* With the `std` feature, it implements [`std::io::Write`][__link3]
* With the `with_serde_cbor` feature, it uses [`serde_cbor`][__link4]’s trait unsealing feature to
  implement its [`Write`][__link5] trait.
* Likewise, there are features for ciborium and minicbor. The minicbor version is a bit special
  in that there is both a `with_minicbor` / `with_minicbor_0_19` feature.
* Starting at `with_minicbor_0_19`, features carry a version. This allows users of different
  minicbor versions to coexist in the same crate, and moreover ensures that the dependencies
  expressed in the Cargo.toml files to describe the requirements precisely.
* With the `with_embedded_io_0_4`, `_0_6` and `_async_0_6` features, the Write trait of
  `embedded-io` / `-async` is implemented.

### Crate size

Compared to the original plan of “doing one thing, and doing that right”, this crate has grown
a bit, in that it contains trait implementations for several serializers, and extra mechanism
for combining the own writer with others (from cryptographic digests or CRCs). Both these are
temporary – once there is a 1.0 version of embedded-io, the Tee will be split out into a
dedicated crate (with only compatibility re-exports and constructors / destructors remaining),
and once serializers start using the stable embedded-io, no more writer implementations will
need to be added.


 [__cargo_doc2readme_dependencies_info]: ggGkYW0BYXSEG_W_Gn_kaocAGwCcVPfenh7eGy6gYLEwyIe4G6-xw_FwcbpjYXKEG5ujMtT8eh3CG3pdb_STVaaOG4vLhtt63PS9G6Cfgm2I0GhwYWSCgmpzZXJkZV9jYm9yZjAuMTEuMoNxd2luZG93ZWQtaW5maW5pdHllMC4xLjdxd2luZG93ZWRfaW5maW5pdHk
 [__link0]: https://docs.rs/windowed-infinity/0.1.7/windowed_infinity/struct.WindowedInfinity.html
 [__link1]: https://tools.ietf.org/html/rfc7959
 [__link2]: https://docs.rs/windowed-infinity/0.1.7/windowed_infinity/struct.WindowedInfinity.html
 [__link3]: https://doc.rust-lang.org/stable/std/?search=io::Write
 [__link4]: https://crates.io/crates/serde_cbor/0.11.2
 [__link5]: https://docs.rs/serde_cbor/0.11.2/serde_cbor/?search=ser::Write
