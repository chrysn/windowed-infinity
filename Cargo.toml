[package]
name = "windowed-infinity"
version = "0.1.7"
authors = ["chrysn <chrysn@fsfe.org>"]
edition = "2021"
rust-version = "1.75"

description = "A data structure representing an infinite sequentially writable u8 vector of which a small view has writes to it preserved. This is primarily useful when implementing CoAP block-wise transfers, and also convenient for logging on constrained devices."
documentation = "https://docs.rs/windowed-infinity/"
repository = "https://codeberg.org/chrysn/windowed-infinity"
categories = ["no-std::no-alloc"]
license = "MIT OR Apache-2.0"

[features]
std = []
with_serde_cbor = ["serde_cbor", "serde"]
with_minicbor = [ "minicbor" ]
with_minicbor_0_19 = [ "minicbor_0_19" ]
with_minicbor_0_24 = [ "minicbor_0_24" ]
with_minicbor_0_25 = [ "minicbor_0_25" ]
with_minicbor_0_26 = [ "minicbor_0_26" ]
with_ciborium = [ "ciborium-io" ]
with_embedded_io_0_4 = []
with_embedded_io_0_6 = [ "embedded_io_0_6" ]
with_embedded_io_async_0_6 = [ "embedded_io_async_0_6", "embedded_io_0_6" ]

[dependencies]
serde_cbor = { version = "0.11", default-features = false, features = ["unsealed_read_write"], optional = true }
serde = { version = "^1", optional = true, default-features = false }
minicbor = { version = ">= 0.12, < 0.16", optional = true, default-features = false }
minicbor_0_19 = { package = "minicbor", version = "0.19", optional = true, default-features = false }
minicbor_0_24 = { package = "minicbor", version = "0.24.4", optional = true, default-features = false }
minicbor_0_25 = { package = "minicbor", version = "0.25.0", optional = true, default-features = false }
minicbor_0_26 = { package = "minicbor", version = "0.26.0", optional = true, default-features = false }
ciborium-io = { version = "0.2", optional = true, default-features = false }
# Some version of embedded-io is always pulled in to power Tee internally
embedded-io = { version = "0.4", default-features = false }
embedded_io_0_6 = { package = "embedded-io", version = "0.6", optional = true }
embedded_io_async_0_6 = { package = "embedded-io-async", version = "0.6", optional = true }

digest = "0.10"
crc = "3.0"

[dev-dependencies]
ciborium-ll = { version = "0.2", default-features = false }
tokio = { version = "1.24.2", features = [ "macros", "rt-multi-thread" ] }

[package.metadata.docs.rs]
all-features = true
