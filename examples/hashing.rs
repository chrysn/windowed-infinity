use windowed_infinity::WindowedInfinity;

fn write_to(w: &mut impl core::fmt::Write) {
    w.write_str("Lorem ipsum dolor sit amet.").unwrap();
}

fn main() {
    let crc = crc::Crc::<u64>::new(&crc::CRC_64_ECMA_182);

    let mut data = [0u8; 20];
    let mut writer = WindowedInfinity::new(&mut data, 0).tee_crc64(&crc);
    write_to(&mut writer);
    let (writer, etag) = writer.into_windowed_and_crc();
    println!(
        "First cunk is: {:?}, total CRC {:x}",
        std::str::from_utf8(writer.written()),
        etag.finalize()
    );

    let mut writer = WindowedInfinity::new(&mut data, -20).tee_crc64(&crc);
    write_to(&mut writer);
    let (writer, etag) = writer.into_windowed_and_crc();
    println!(
        "Second cunk is: {:?}, total CRC {:x}",
        std::str::from_utf8(writer.written()),
        etag.finalize(),
    );
}
