use windowed_infinity::WindowedInfinity;

fn write_to(w: &mut WindowedInfinity) {
    w.write(b"Lorem ipsum dolor sit amet.");
}

fn main() {
    let mut data = [0u8; 20];
    let mut writer = WindowedInfinity::new(&mut data, 0);
    write_to(&mut writer);
    println!("First cunk is: {:?}", std::str::from_utf8(&data));

    let mut writer = WindowedInfinity::new(&mut data, -20);
    write_to(&mut writer);
    println!(
        "Second chunk is: {:?}",
        std::str::from_utf8(writer.written())
    );
}
