//! Types that encapsulate foreign types into embedded-io writers, suitable for inclusion in a Tee
//!
//! These types are not exposed publicly (only inside a Tee which on its own is not public yet).

use super::MyWrite;

pub struct WritableDigest<D: digest::Digest>(pub(crate) D);

impl<D: digest::Digest> MyWrite for WritableDigest<D> {
    type Error = core::convert::Infallible;

    fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.0.update(buf);
        Ok(())
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl<'a, D: digest::Digest> crate::tee::Tee<crate::WindowedInfinity<'a>, WritableDigest<D>> {
    pub fn into_windowed_and_digest(self) -> (crate::WindowedInfinity<'a>, D) {
        (self.w1, self.w2.0)
    }
}

pub struct WritableCrc<'c, W: crc::Width>(pub(crate) crc::Digest<'c, W>);

impl MyWrite for WritableCrc<'_, u64> {
    type Error = core::convert::Infallible;

    fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.0.update(buf);
        Ok(())
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl MyWrite for WritableCrc<'_, u32> {
    type Error = core::convert::Infallible;

    fn write(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.0.update(buf);
        Ok(())
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl<'a, 'c> crate::tee::Tee<crate::WindowedInfinity<'a>, WritableCrc<'c, u64>> {
    pub fn into_windowed_and_crc(self) -> (crate::WindowedInfinity<'a>, crc::Digest<'c, u64>) {
        (self.w1, self.w2.0)
    }
}

impl<'a, 'c> crate::tee::Tee<crate::WindowedInfinity<'a>, WritableCrc<'c, u32>> {
    pub fn into_windowed_and_crc(self) -> (crate::WindowedInfinity<'a>, crc::Digest<'c, u32>) {
        (self.w1, self.w2.0)
    }
}
